#!/bin/bash
#OAR -p gpu='YES' and cluster='dellt630gpu' and host='nefgpu14.inria.fr'
#OAR -l/gpunum=1,walltime=1
#OAR --notify mail:juan-diego.gonzales-zuniga@inria.fr

module load cuda/8.0 cudnn/6.0-cuda-8.0
module load cuda/8.0 cudnn/6.0-cuda-8.0
module load cuda/8.0 cudnn/6.0-cuda-8.0

python test_calltech.py --run_name=BlitzNet512_COCO+VOC07+12 --dataset=calltech --split=test --image_size=512 --detect --ckpt=1 --set_n '06' --v_n '00'

