import logging
import os
import cv2

import numpy as np
import xml.etree.ElementTree as ET

from PIL import Image

from paths import DATASETS_ROOT

log = logging.getLogger()

VOC_CATS = ['__background__', 'aeroplane', 'bicycle', 'bird', 'boat', 'bottle',
            'bus', 'car', 'cat', 'chair', 'cow', 'diningtable', 'dog', 'horse',
            'motorbike', 'person', 'pottedplant', 'sheep', 'sofa', 'train',
            'tvmonitor']


class CallTechLoader():
    def __init__(self, seq, V0, split, segmentation=False, augmented_seg=False):
        super().__init__()
        assert seq in ['00', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10']
        assert V0 in ['00', '01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '16', '15', '17', '18', '19', '20']

        self.dataset = 'inria'
        self.seq = seq
        self.V0  = V0
        self.root = os.path.join(DATASETS_ROOT, 'data-USA/')
        self.split = split
        assert split in ['train', 'val', 'trainval', 'test']

        cats = VOC_CATS
        self.cats_to_ids = dict(map(reversed, enumerate(cats)))
        self.ids_to_cats = dict(enumerate(cats))
        self.num_classes = len(cats)
        self.categories = cats[1:]

        self.segmentation = segmentation
        self.augmented_seg = augmented_seg
        assert not self.segmentation or self.segmentation and self.seq == '01'
        
        self.set_filenames()
        log.info("Created a loader Inria seq%s %s with %i images" % (seq, split, len(self.filenames)))

    def set_filenames(self):
        images = os.listdir("%simages/set%s/V0%s" % (self.root, self.seq, self.V0))

        nameImages = []
        listImages = []
        for i in range(len(images)):
             nameImages = images[i].split(".")
             if nameImages[1] == 'jpg' :
                listImages.append(nameImages[0])

        listImages.sort()
        self.filenames = listImages[:-1]

    def load_image(self, name):
        im = Image.open('%simages/set%s/V0%s/%s.jpg' % (self.root, self.seq, self.V0, name)).convert('RGB')
        im = np.array(im) / 255.0
        im = im.astype(np.float32)
        return im

    def load_image_2(self, path):
        im = Image.open(path).convert('RGB')
        im = np.array(im) / 255.0
        im = im.astype(np.float32)
        return im

    def get_filenames(self):
        return self.filenames

    def read_annotations(self, name):
        bboxes = []
        cats = []

        im = Image.open('%simages/set%s/V0%s/%s.jpg' % (self.root, self.seq, self.V0, name))
        width, height = im.size
        difficulty = []
        array = []
        with open('%sannotations/set%s/V0%s/%s.txt' % (self.root, self.seq, self.V0, name),'r') as f:
             lines = f.read().split('\n')[:-1]

             for i in range(1,len(lines)):
                 array = lines[i].split(" ")
                 difficult = 0.5
                 difficulty.append(difficult)
                 cat = array[0]
                 cats.append(cat)
                 x = int(array[1])
                 y = int(array[2])
                 w = int(array[3]) - x
                 h = int(array[4]) - y
                 bboxes.append((x, y, w, h))
	 
        gt_cats = np.array(cats)
        gt_bboxes = np.array(bboxes).reshape((len(bboxes), 4))
        difficulty = np.array(difficulty)
        
        seg_gt = self.read_segmentations(name, height, width)

        output = gt_bboxes, seg_gt, gt_cats, width, height, difficulty
       
        return output

    def read_segmentations(self, name, height, width):
        if self.segmentation:
            try:
                seg_folder = self.root + 'SegmentationClass/'
                seg_file = seg_folder + name + '.png'
                seg_map = Image.open(seg_file)
            except:
                assert self.augmented_seg
                seg_folder = self.root + 'SegmentationClassAug/'
                seg_file = seg_folder + name + '.png'
                seg_map = Image.open(seg_file)
            segmentation = np.array(seg_map, dtype=np.uint8)
        else:
            # if there is no segmentation for a particular image we fill the mask
            # with zeros to keep the same amount of tensors but don't learn from it
            segmentation = np.zeros([height, width], dtype=np.uint8)
        return segmentation
