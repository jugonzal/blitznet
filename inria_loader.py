import logging
import os
import cv2

import numpy as np
import xml.etree.ElementTree as ET

from PIL import Image

from paths import DATASETS_ROOT

log = logging.getLogger()

VOC_CATS = ['__background__', 'aeroplane', 'bicycle', 'bird', 'boat', 'bottle',
            'bus', 'car', 'cat', 'chair', 'cow', 'diningtable', 'dog', 'horse',
            'motorbike', 'person', 'pottedplant', 'sheep', 'sofa', 'train',
            'tvmonitor']


class INRIALoader():
    def __init__(self, seq, split, segmentation=False, augmented_seg=False):
        super().__init__()
        assert seq in ['00', '01']
        self.dataset = 'inria'
        self.year = 17
        self.seq = seq
        self.root = os.path.join(DATASETS_ROOT, 'data-INRIA/')
        self.split = split
        assert split in ['train', 'val', 'trainval', 'test']

        cats = VOC_CATS
        self.cats_to_ids = dict(map(reversed, enumerate(cats)))
        self.ids_to_cats = dict(enumerate(cats))
        self.num_classes = len(cats)
        self.categories = cats[1:]

        self.segmentation = segmentation
        self.augmented_seg = augmented_seg

        assert not self.segmentation or self.segmentation and self.seq == '01'

        if self.augmented_seg:
            filelist = 'ImageSets/SegmentationAug/%s.txt'
        elif self.segmentation:
            filelist = 'ImageSets/Segmentation/%s.txt'
        else:
            filelist = 'ImageSets/Main/%s.txt'
        with open(os.path.join(self.root, filelist % self.split), 'r') as f:
            self.filenames = f.read().split('\n')[:-1]
        log.info("Created a loader Inria seq%s %s with %i images" % (seq, split, len(self.filenames)))

    def load_image(self, name):
        im = Image.open('%sPNGImages/set%s/V000/%s.png' % (self.root, self.seq, name)).convert('RGB')
        im = np.array(im) / 255.0
        im = im.astype(np.float32)
        return im

    def load_image_2(self, path):
        im = Image.open(path).convert('RGB')
        im = np.array(im) / 255.0
        im = im.astype(np.float32)
        return im

    def get_filenames(self):
        return self.filenames

    def read_annotations(self, name):
        bboxes = []
        cats = []

        im = Image.open('%sPNGImages/set%s/V000/%s.png' % (self.root, self.seq, name))
        width, height = im.size
        difficulty = []
        array = []
        with open('%sAnnotations/set%s/V000/%s.txt' % (self.root, self.seq, name),'r') as f:
             lines = f.read().split('\n')[:-1]

             for i in range(1,len(lines)):
                 array = lines[i].split(" ")
                 difficult = 0.5
                 difficulty.append(difficult)
                 cat = self.cats_to_ids[array[0]]
                 cats.append(cat)
                 x = int(array[1])
                 y = int(array[2])
                 w = int(array[3]) - x
                 h = int(array[4]) - y
                 bboxes.append((x, y, w, h))
	 
        gt_cats = np.array(cats)
        gt_bboxes = np.array(bboxes).reshape((len(bboxes), 4))
        difficulty = np.array(difficulty)
        
        seg_gt = self.read_segmentations(name, height, width)

        output = gt_bboxes, seg_gt, gt_cats, width, height, difficulty
       
        return output

    def read_segmentations(self, name, height, width):
        if self.segmentation:
            try:
                seg_folder = self.root + 'SegmentationClass/'
                seg_file = seg_folder + name + '.png'
                seg_map = Image.open(seg_file)
            except:
                assert self.augmented_seg
                seg_folder = self.root + 'SegmentationClassAug/'
                seg_file = seg_folder + name + '.png'
                seg_map = Image.open(seg_file)
            segmentation = np.array(seg_map, dtype=np.uint8)
        else:
            # if there is no segmentation for a particular image we fill the mask
            # with zeros to keep the same amount of tensors but don't learn from it
            segmentation = np.zeros([height, width], dtype=np.uint8)
        return segmentation
